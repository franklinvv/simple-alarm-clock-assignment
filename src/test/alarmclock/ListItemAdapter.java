package test.alarmclock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * This class defines the array adapter for the main list view.
 * @author franklin
 *
 */
public class ListItemAdapter extends ArrayAdapter<Alarm> {
	private SimpleDateFormat mFormatter = new SimpleDateFormat("HH:mm");
	private ArrayList<Alarm> mAlarms;
	private LayoutInflater mInflater;
	private int mTextViewTitleId;

	public ListItemAdapter(Context context, int textViewResourceId, ArrayList<Alarm> alarms) {
		super(context, textViewResourceId, alarms);
		this.mAlarms = alarms;
		this.mTextViewTitleId = textViewResourceId;
		this.mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if(convertView == null) {
			convertView = mInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
			holder = new ViewHolder();
			holder.title = (TextView)convertView.findViewById(mTextViewTitleId);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}

		Alarm alarm = mAlarms.get(position);
		holder.title.setText(mFormatter.format(alarm.getCalendar().getTime()));

		return convertView;
	}
	
	static class ViewHolder {
		int idInDatabase;
		TextView title;
	}
}
