package test.alarmclock;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TimePicker;

/**
 * The main activity extends ListActivity instead of just Activity. This enables us to make easier use of the ListView that
 * the layout for this Activity contains.
 * @author franklin
 *
 */
public class AlarmClockActivity extends ListActivity {
	private static final int DIALOG_ADD_ALARM = 1;
	private SimpleDateFormat mFormatter = new SimpleDateFormat("HH:mm");
	private ListView mListView;
	private ListItemAdapter mListAdapter;

	/**
	 * Listener object that is used when the time is being set with the time picker
	 */
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// add the time to your database
			AlarmsDatabase adb = new AlarmsDatabase(AlarmClockActivity.this);
			// (read section 2.3) ...

			// insertedId should get the id of the inserted alarm in the database
			long insertedId = -1;

			// check whether the alarm has been inserted
			if(insertedId != -1) {
				// add the inserted alarm object to the adapter, and refresh the list view
				mListAdapter.add(adb.getAlarm(insertedId));
				mListAdapter.notifyDataSetChanged();

				// send an intent to have the alarm scheduler reschedule its alarms
				// (read section 2.4) ...
			}
		}
	};

	/**
	 * Listener object that is used when an item in the list view is being clicked
	 */
	private OnItemClickListener mItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> av, View v, final int position,
				long arg3) {
			// create a dialog builder and set the title and message
			Builder builder = new AlertDialog.Builder(AlarmClockActivity.this);
			builder.setTitle(R.string.dlg_delete_title);
			String timeString = mFormatter.format(mListAdapter.getItem(position).getCalendar().getTime());
			String message = String.format(getString(R.string.dlg_delete_body), timeString);
			builder.setMessage(message);

			// set the positive and negative buttons of the dialog
			builder.setPositiveButton(android.R.string.yes, new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					deleteAlarmFromDBAndAdapter(position);
				}
			});
			builder.setNegativeButton(android.R.string.no, new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});

			// show the dialog
			builder.show();
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarmclock);
		AlarmsDatabase adb = new AlarmsDatabase(this);

		// get a reference to the list view in this activity
		mListView = getListView();

		// set an array adapter as the data source for the list view
		mListAdapter = new ListItemAdapter(this, android.R.id.text1, adb.getAlarms());
		setListAdapter(mListAdapter);

		// define a listener for the click event for an item
		mListView.setOnItemClickListener(mItemClickListener);
	}

	/**
	 * Delete an alarm from the database and remove it from the adapter as well.
	 * @param position 
	 */
	private void deleteAlarmFromDBAndAdapter(int position) {
		AlarmsDatabase adb = new AlarmsDatabase(this);

		// get an alarm object from the array adapter, at the position that is being selected in the list view by the user
		// (read section 2.5) ...
		Alarm alarm = new Alarm();

		/**
		 * Delete the alarm from the database and remove it from the adapter. So, delete it in
		 * two separate locations.
		 */
		// (read section 2.6) ...
		// notify the adapter that the data has been changed, so the corresponding list view refreshes itself

		AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
		// don't forget to cancel this alarm as well, else the alarm manager will still keep a reference to the non-existing alarm
		// and fire it at the time of this alarm
		// (read section 2.7) ...
	}

	/**
	 * Code to be executed when the add alarm button is clicked
	 * @param v
	 */
	public void onAddAlarmClick(View v) {
		// show the time picker dialog
		// (read section 2.1) ...
	}

	/**
	 * Is called automatically when you use showDialog() 
	 */
	@Override
	public Dialog onCreateDialog(int id) {
		switch(id) {
		case DIALOG_ADD_ALARM:
			// return a time picker dialog, with default time set to now
			Calendar now = Calendar.getInstance();
			TimePickerDialog tpd = new TimePickerDialog(this,
					mTimeSetListener, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
			tpd.setTitle(R.string.dlg_time_title);

			/**
			 * When cancel is clicked in this dialog, dismiss() is being called. To remove the cached dialog (and make sure that it
			 * is being recreated each and every time), we need to remove the dialog as well when it is being dismissed.
			 */
			// (read section 2.2) ...
			
			return tpd;
		}

		return super.onCreateDialog(id);
	}
}