package test.alarmclock;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * This class takes care of the scheduling of all alarms.
 * @author franklin
 *
 */
public class AlarmScheduler extends BroadcastReceiver {
	// Here we define the intent string. It's pretty common to define it with the package name as the prefix.
	public final static String INTENT_SCHEDULE_ALARMS = "test.alarmclock.SCHEDULE_ALARMS";
	private final static int MILLISECONDS_IN_ONE_DAY = 1000 * 60 * 60 * 24;

	/**
	 * This method is called when this broadcast receiver receives an intent that it has to process.
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("AlarmScheduler", "Rescheduling all alarms");
		
		// get the alarm manager
		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		// get all alarms from the database
		AlarmsDatabase adb = new AlarmsDatabase(context);
		// (read section 5.1) ...
		ArrayList<Alarm> alarms = new ArrayList<Alarm>();
		
		// iterate over the alarms
		for (Alarm alarm : alarms) {
			
			/**
			 * Set up a pending intent, and use the alarm id from the database as the request code for the pending intent. This way, we
			 * can uniquely identify the separate pending intents because we will probably have more than one alarm in our database. 
			 * 
			 * If you want to cancel an alarm, you have to recreate a similar PendingIntent and 'select' the right alarm by
			 * passing the right requestCode.
			 */
			Intent notificationIntent = new Intent(context, AlarmClockFireActivity.class);
	        PendingIntent pendingIntent = PendingIntent.getActivity(context, (int)alarm.getId(), notificationIntent, 0);
			alarmManager.cancel(pendingIntent);

			/**
			 * Get the alarm date and time in milliseconds. In the database class, we defined that we should always have a date that is
			 * at most 1 full day ahead, so the date can be in the past. However, the time that is set along with the date is
			 * correct. Therefore, we have to add 1 exact day iteratively, until we have a calendar date that is within the next 24 hours.
			 */
			// (read section 5.2) ...
			Calendar now = Calendar.getInstance(); // the current date/time
			Calendar alarmCalendar = alarm.getCalendar(); // the date/time from the database
			
			/**
			 *  Now, alarmCalendar has the date and time of the next time the alarm should be triggered. Therefore, we can use this
			 *  date and time to define when this alarm should first be triggered.
			 */
			// (read section 5.3) ...
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis(), MILLISECONDS_IN_ONE_DAY, pendingIntent);
		}
	}

}
