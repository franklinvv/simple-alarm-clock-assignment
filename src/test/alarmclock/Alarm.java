package test.alarmclock;

import java.util.Calendar;

/**
 * This is a class that represents alarm objects in the database, and uses public member variables for easy of usability.
 * @author franklin
 *
 */
public class Alarm {
	private long mId;
	private Calendar mCalendar;
	
	public long getId() {
		return mId;
	}

	public void setId(long id) {
		this.mId = id;
	}

	public Calendar getCalendar() {
		return mCalendar;
	}

	public void setCalendar(Calendar calendar) {
		this.mCalendar = calendar;
	}

	/**
	 * empty constructor
	 */
	public Alarm() {

	}
	
	/**
	 * Simple constructor with parameters
	 * @param id The id that this alarm has in the database
	 * @param calendar A calendar object that represents the time in the database 
	 */
	public Alarm(long id, Calendar calendar) {
		this.mId = id;
		this.mCalendar = calendar;
	}
}
