package test.alarmclock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * All interaction with the database is managed with this helper class.
 * 
 * @author franklin
 *
 */
public class AlarmsDatabase extends SQLiteOpenHelper {
	private SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String DB_NAME = "Alarms";
	private static final int DB_VERSION = 1;
	private static final String ALARM_TABLE = "alarm";
	private static final String COLUMN_TIME = "time";
	
	// (read section 3.1) ...
	private static final String DB_CREATE = "?";
	
	/**
	 * Simple constructor that calls the constructor of the super class along with added parameters. Always instantiate
	 * this class with this constructor.
	 * @param context
	 */
	public AlarmsDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	/**
	 * Should not be called.
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DB_CREATE);
	}
	
	/**
	 * Add a new alarm to the database.
	 * @param calendar A calendar representation of the time for the alarm. The calendar date should always be in the past or less than 1 full
	 * day ahead! How far in the past does not matter, as long as it indeed is in the past or at most 1 full day ahead. This is because
	 * SQLite does not support TIME types. It just supports DATETIME types. Therefore, we have to specify the time along with a date.
	 * @return The inserted id in the database, or -1 if it wasn't inserted properly
	 */
	public long addAlarm(Calendar calendar) {
		SQLiteDatabase db = getWritableDatabase();
		long insertedId;
		
		try {
			// begin a transaction
			db.beginTransaction();
			String calendarString = mFormatter.format(calendar.getTime());
			
			/**
			 * Check whether the calendar object passed to this method is at MOST 1 full day ahead of the current date and time. If not,
			 * throw an exception.
			 */
			Calendar tomorrow = Calendar.getInstance();
			tomorrow.add(Calendar.DAY_OF_YEAR, 1);
			if(calendar.after(tomorrow)) {
				throw new SQLException(String.format("Can't add %1$s: calendar date is not in the past", calendarString));
			}
			
			/**
			 * Define the content values object with the values that need to be inserted. In this case, it's just a datetime string.
			 */
			ContentValues cv = new ContentValues();
			cv.put(COLUMN_TIME, calendarString);
			insertedId = db.insert(ALARM_TABLE, null, cv);
			
			// if we came all the way here, the transaction can be set to the successful state
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
			insertedId = -1;
		} finally {
			db.endTransaction();
			db.close();
		}
		
		return insertedId;
	}
	
	/**
	 * Get an alarm from the database, for a specific alarm id.
	 * @param id The id of an alarm in the database.
	 * @return An alarm object or null if the id does not exist in the database.
	 */
	public Alarm getAlarm(long id) {
		SQLiteDatabase db = getReadableDatabase();
		Alarm alarm = null;
		
		try {
			// (read section 3.2) ...
			// Use the mFormatter object to convert dates to strings and reverse (using the format() and parse() methods in this object)

		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			db.close();
		}
		
		return alarm;
	}
	
	/**
	 * Get a list of alarms.
	 * @return An array list containing all alarm objects.
	 */
	public ArrayList<Alarm> getAlarms() {
		SQLiteDatabase db = getReadableDatabase();
		
		// create the array list that we want to return
		ArrayList<Alarm> alarms = new ArrayList<Alarm>();
		
		try {
			// (read section 3.3)
			// again, mFormatter can be used for converting dates to strings and reverse
		} catch (SQLException e) {
			e.printStackTrace();			
		} finally {
			db.close();
		}
		
		return alarms;
	}
	
	/**
	 * Delete an alarm object.
	 * @param alarm The alarm object that needs to be deleted from the database.
	 */
	public void deleteAlarm(Alarm alarm) {
		SQLiteDatabase db = getWritableDatabase();

		try {
			db.beginTransaction();
			
			/**
			 * Delete the specific row with the right id from the alarm table
			 */
			// (read section 3.4) ...
			
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("AlarmsDatabase", "Error deleting data: " + e.toString());
		} finally {
			db.endTransaction();
			close();
		}

	}

	/**
	 * Should not be called.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try {
			db.beginTransaction();
			String sql = "DROP TABLE IF EXISTS " + ALARM_TABLE;
			db.execSQL(sql);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}

		onCreate(db);
	}

}
