package test.alarmclock;

import java.io.IOException;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

/**
 * This activity is shown when an alarm is triggered.
 * @author franklin
 *
 */
public class AlarmClockFireActivity extends Activity {
	private MediaPlayer mMediaPlayer;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarmclockfire);
		
		/**
		 * Prepare an alarm sound when the alarm is fired, in the alarm stream. Note: this code is not enough to actually play the sound!
		 */
		// (read section 4.2) ...
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
		
		String uri = "android.resource://" + getPackageName() + "/" + R.raw.alarm;
		try {
			mMediaPlayer.setDataSource(this, Uri.parse(uri));
			mMediaPlayer.prepareAsync();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Stop playing the alarm sound when the stop button is clicked
	 * @param v
	 */
	public void onStopAlarmClick(View v) {
		// (read section 4.1) ...
		
		if(mMediaPlayer != null) {
			mMediaPlayer.stop();
		}
	}
}
